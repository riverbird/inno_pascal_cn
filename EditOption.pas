unit EditOption;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons, RXSwitch;

type
  TfrmEditOption = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Image1: TImage;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    RxSwitch1: TRxSwitch;
    RxSwitch2: TRxSwitch;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEditOption: TfrmEditOption;

implementation

uses Main;

{$R *.dfm}

procedure TfrmEditOption.BitBtn1Click(Sender: TObject);
begin
  if rxswitch1.stateon=true then
    mainform.XPMenu1.Active:=true
  else
    mainform.XPMenu1.Active:=false;

  if rxswitch2.stateon=true then
    mainform.Memo.Gutter.ShowLineNumbers:=true
  else
    mainform.Memo.Gutter.ShowLineNumbers:=false;

  self.Close;
end;

procedure TfrmEditOption.BitBtn2Click(Sender: TObject);
begin
  self.Close;
end;

end.
