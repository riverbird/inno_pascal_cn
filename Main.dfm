object MainForm: TMainForm
  Left = 204
  Top = 112
  Width = 405
  Height = 350
  Caption = '*'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = True
  Position = poDefault
  Scaled = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object TopDock: TDock97
    Left = 0
    Top = 0
    Width = 397
    Height = 27
    BoundLines = [blTop]
    object MainToolbar: TToolbar97
      Left = 0
      Top = 0
      Caption = 'Main'
      DefaultDock = TopDock
      DockPos = 0
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      object NewButton: TToolbarButton97
        Left = 0
        Top = 0
        Width = 23
        Height = 22
        Hint = 'New'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          1800000000000003000000000000000000000000000000000000C6C7C6C6C7C6
          C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7
          C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6
          C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6
          0000000000000000000000000000000000000000000000000000000000000000
          00C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6
          000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6
          000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6
          000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6
          000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
          00C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF000000000000000000000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6
          000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000C6C7
          C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000FFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFF000000000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6
          000000000000000000000000000000000000000000000000C6C7C6C6C7C6C6C7
          C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6
          C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6}
        GlyphMask.Data = {00000000}
        OnClick = FNewClick
      end
      object OpenButton: TToolbarButton97
        Left = 23
        Top = 0
        Width = 23
        Height = 22
        Hint = 'Open'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          1800000000000003000000000000000000000000000000000000C6C7C6C6C7C6
          C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7
          C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6
          C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000000000
          000000000000000000000000000000000000000000000000000000C6C7C6C6C7
          C6C6C7C6C6C7C6C6C7C600000000000000868400868400868400868400868400
          8684008684008684008684000000C6C7C6C6C7C6C6C7C6C6C7C600000000FFFF
          0000000086840086840086840086840086840086840086840086840086840000
          00C6C7C6C6C7C6C6C7C6000000FFFFFF00FFFF00000000868400868400868400
          8684008684008684008684008684008684000000C6C7C6C6C7C600000000FFFF
          FFFFFF00FFFF0000000086840086840086840086840086840086840086840086
          84008684000000C6C7C6000000FFFFFF00FFFFFFFFFF00FFFF00000000000000
          000000000000000000000000000000000000000000000000000000000000FFFF
          FFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFF000000C6C7C6C6C7
          C6C6C7C6C6C7C6C6C7C6000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFF
          FFFF00FFFFFFFFFF000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C600000000FFFF
          FFFFFF00FFFF000000000000000000000000000000000000000000C6C7C6C6C7
          C6C6C7C6C6C7C6C6C7C6C6C7C6000000000000000000C6C7C6C6C7C6C6C7C6C6
          C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000000000000000C6C7C6C6C7C6C6C7C6
          C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7
          C6000000000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6
          C7C6000000C6C7C6C6C7C6C6C7C6000000C6C7C6000000C6C7C6C6C7C6C6C7C6
          C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000000000000000C6C7
          C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6
          C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6}
        GlyphMask.Data = {00000000}
        OnClick = FOpenClick
      end
      object SaveButton: TToolbarButton97
        Left = 46
        Top = 0
        Width = 23
        Height = 22
        Hint = 'Save'
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          1800000000000003000000000000000000000000000000000000008600008600
          0086000086000086000086000086000086000086000086000086000086000086
          0000860000860000860000860000860000000000000000000000000000000000
          0000000000000000000000000000000000000000000000008600008600000000
          008684008684000000000000000000000000000000000000C6C7C6C6C7C60000
          0000868400000000860000860000000000868400868400000000000000000000
          0000000000000000C6C7C6C6C7C6000000008684000000008600008600000000
          008684008684000000000000000000000000000000000000C6C7C6C6C7C60000
          0000868400000000860000860000000000868400868400000000000000000000
          0000000000000000000000000000000000008684000000008600008600000000
          0086840086840086840086840086840086840086840086840086840086840086
          8400868400000000860000860000000000868400868400000000000000000000
          0000000000000000000000000000008684008684000000008600008600000000
          008684000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C60000
          00008684000000008600008600000000008684000000C6C7C6C6C7C6C6C7C6C6
          C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000008684000000008600008600000000
          008684000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C60000
          00008684000000008600008600000000008684000000C6C7C6C6C7C6C6C7C6C6
          C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000008684000000008600008600000000
          008684000000C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C6C6C7C60000
          00000000000000008600008600000000008684000000C6C7C6C6C7C6C6C7C6C6
          C7C6C6C7C6C6C7C6C6C7C6C6C7C6000000C6C7C6000000008600008600000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000860000860000860000860000860000860000860000860000
          8600008600008600008600008600008600008600008600008600}
        GlyphMask.Data = {00000000}
        OnClick = FSaveClick
      end
      object MainSep1: TToolbarSep97
        Left = 69
        Top = 0
      end
      object CompileButton: TToolbarButton97
        Left = 75
        Top = 0
        Width = 23
        Height = 22
        Action = actCompile
        DisplayMode = dmGlyphOnly
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          04000000000080000000C40E0000C40E00001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
          88888888888888888888800000000000000080FFFFFFFFFFFFF080F0F0F0F0F0
          F0F080FFFFFFFFFFFFF080F0F0F0F0F0F0F080FFFFFFFFFFFFF0800000000000
          0000884888884888884884448884448884448848484848484848884888884888
          8848888888888888888888488888488888488888888888888888}
        GlyphMask.Data = {00000000}
      end
      object RunButton: TToolbarButton97
        Left = 104
        Top = 0
        Width = 23
        Height = 22
        Action = actRun
        DisplayMode = dmGlyphOnly
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000026000000120000000100
          04000000000068010000C40E0000C40E00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333300333333333333333333333333333333333333
          330033333333333333333333333333333333333333003338F333333333333333
          3333333333333333330033380FF33333333333333333FF333333333333003338
          077FF3333333333333388FFF33333333330033380AA77FF3333333333338888F
          FF333333330033380AAAA77FF3333333333888888FFF3333330033380AAAAAA7
          7F33333333388888888FFF33330033380AAAAAAAA00333333338888888888333
          330033380AAAAAA0088333333338888888833333330033380AAAA00883333333
          3338888883333333330033380AA0088333333333333888833333333333003338
          0008833333333333333883333333333333003338088333333333333333333333
          3333333333003338833333333333333333333333333333333300333333333333
          3333333333333333333333333300333333333333333333333333333333333333
          3300}
        GlyphMask.Data = {00000000}
        NumGlyphs = 2
      end
      object StopButton: TToolbarButton97
        Left = 127
        Top = 0
        Width = 23
        Height = 22
        Action = actStop
        DisplayMode = dmGlyphOnly
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000026000000120000000100
          04000000000068010000CE0E0000C40E00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333300333333333333333333333333333333333333
          330033333333333333333333333333333333333333003333FFFFFFFFFFF33333
          333FFFFFFFFFF333330033380777777777F33333338888888888F33333003338
          0999999997F33333338888888888F333330033380999999997F3333333888888
          8888F333330033380999999997F33333338888888888F3333300333809999999
          97F33333338888888888F333330033380999999997F33333338888888888F333
          330033380999999997F33333338888888888F333330033380999999997F33333
          338888888888F333330033380999999997F33333338888888888F33333003338
          0000000007F33333338888888888333333003338888888888833333333333333
          3333333333003333333333333333333333333333333333333300333333333333
          3333333333333333333333333300333333333333333333333333333333333333
          3300}
        GlyphMask.Data = {00000000}
        NumGlyphs = 2
      end
      object ToolbarSep971: TToolbarSep97
        Left = 98
        Top = 0
      end
    end
  end
  object RightDock: TDock97
    Left = 388
    Top = 27
    Width = 9
    Height = 221
    Position = dpRight
  end
  object BottomDock: TDock97
    Left = 0
    Top = 248
    Width = 397
    Height = 9
    Position = dpBottom
  end
  object LeftDock: TDock97
    Left = 0
    Top = 27
    Width = 9
    Height = 221
    Position = dpLeft
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 257
    Width = 397
    Height = 20
    Panels = <
      item
        Alignment = taCenter
        Text = '   1:   1'
        Width = 64
      end
      item
        Alignment = taCenter
        Width = 64
      end
      item
        Alignment = taCenter
        Text = 'Insert'
        Width = 64
      end
      item
        Width = 50
      end>
  end
  object OuterPanel: TPanel
    Left = 9
    Top = 27
    Width = 379
    Height = 221
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter: TSplitter
      Left = 0
      Top = 170
      Width = 379
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ResizeStyle = rsUpdate
    end
    object Memo: TSynMemo
      Left = 0
      Top = 0
      Width = 379
      Height = 170
      Cursor = crIBeam
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      PopupMenu = MemoPopup
      TabOrder = 0
      Gutter.ShowLineNumbers = True
      Highlighter = Highlighter
      Keystrokes = <
        item
          Command = ecUp
          ShortCut = 38
          ShortCut2 = 0
        end
        item
          Command = ecSelUp
          ShortCut = 8230
          ShortCut2 = 0
        end
        item
          Command = ecScrollUp
          ShortCut = 16422
          ShortCut2 = 0
        end
        item
          Command = ecDown
          ShortCut = 40
          ShortCut2 = 0
        end
        item
          Command = ecSelDown
          ShortCut = 8232
          ShortCut2 = 0
        end
        item
          Command = ecScrollDown
          ShortCut = 16424
          ShortCut2 = 0
        end
        item
          Command = ecLeft
          ShortCut = 37
          ShortCut2 = 0
        end
        item
          Command = ecSelLeft
          ShortCut = 8229
          ShortCut2 = 0
        end
        item
          Command = ecWordLeft
          ShortCut = 16421
          ShortCut2 = 0
        end
        item
          Command = ecSelWordLeft
          ShortCut = 24613
          ShortCut2 = 0
        end
        item
          Command = ecRight
          ShortCut = 39
          ShortCut2 = 0
        end
        item
          Command = ecSelRight
          ShortCut = 8231
          ShortCut2 = 0
        end
        item
          Command = ecWordRight
          ShortCut = 16423
          ShortCut2 = 0
        end
        item
          Command = ecSelWordRight
          ShortCut = 24615
          ShortCut2 = 0
        end
        item
          Command = ecPageDown
          ShortCut = 34
          ShortCut2 = 0
        end
        item
          Command = ecSelPageDown
          ShortCut = 8226
          ShortCut2 = 0
        end
        item
          Command = ecPageBottom
          ShortCut = 16418
          ShortCut2 = 0
        end
        item
          Command = ecSelPageBottom
          ShortCut = 24610
          ShortCut2 = 0
        end
        item
          Command = ecPageUp
          ShortCut = 33
          ShortCut2 = 0
        end
        item
          Command = ecSelPageUp
          ShortCut = 8225
          ShortCut2 = 0
        end
        item
          Command = ecPageTop
          ShortCut = 16417
          ShortCut2 = 0
        end
        item
          Command = ecSelPageTop
          ShortCut = 24609
          ShortCut2 = 0
        end
        item
          Command = ecLineStart
          ShortCut = 36
          ShortCut2 = 0
        end
        item
          Command = ecSelLineStart
          ShortCut = 8228
          ShortCut2 = 0
        end
        item
          Command = ecEditorTop
          ShortCut = 16420
          ShortCut2 = 0
        end
        item
          Command = ecSelEditorTop
          ShortCut = 24612
          ShortCut2 = 0
        end
        item
          Command = ecLineEnd
          ShortCut = 35
          ShortCut2 = 0
        end
        item
          Command = ecSelLineEnd
          ShortCut = 8227
          ShortCut2 = 0
        end
        item
          Command = ecEditorBottom
          ShortCut = 16419
          ShortCut2 = 0
        end
        item
          Command = ecSelEditorBottom
          ShortCut = 24611
          ShortCut2 = 0
        end
        item
          Command = ecToggleMode
          ShortCut = 45
          ShortCut2 = 0
        end
        item
          Command = ecCopy
          ShortCut = 16429
          ShortCut2 = 0
        end
        item
          Command = ecPaste
          ShortCut = 8237
          ShortCut2 = 0
        end
        item
          Command = ecDeleteChar
          ShortCut = 46
          ShortCut2 = 0
        end
        item
          Command = ecCut
          ShortCut = 8238
          ShortCut2 = 0
        end
        item
          Command = ecDeleteLastChar
          ShortCut = 8
          ShortCut2 = 0
        end
        item
          Command = ecDeleteLastChar
          ShortCut = 8200
          ShortCut2 = 0
        end
        item
          Command = ecDeleteLastWord
          ShortCut = 16392
          ShortCut2 = 0
        end
        item
          Command = ecUndo
          ShortCut = 32776
          ShortCut2 = 0
        end
        item
          Command = ecRedo
          ShortCut = 40968
          ShortCut2 = 0
        end
        item
          Command = ecLineBreak
          ShortCut = 13
          ShortCut2 = 0
        end
        item
          Command = ecSelectAll
          ShortCut = 16449
          ShortCut2 = 0
        end
        item
          Command = ecCopy
          ShortCut = 16451
          ShortCut2 = 0
        end
        item
          Command = ecBlockIndent
          ShortCut = 24649
          ShortCut2 = 0
        end
        item
          Command = ecLineBreak
          ShortCut = 16461
          ShortCut2 = 0
        end
        item
          Command = ecInsertLine
          ShortCut = 16462
          ShortCut2 = 0
        end
        item
          Command = ecDeleteWord
          ShortCut = 16468
          ShortCut2 = 0
        end
        item
          Command = ecBlockUnindent
          ShortCut = 24661
          ShortCut2 = 0
        end
        item
          Command = ecPaste
          ShortCut = 16470
          ShortCut2 = 0
        end
        item
          Command = ecCut
          ShortCut = 16472
          ShortCut2 = 0
        end
        item
          Command = ecDeleteLine
          ShortCut = 16473
          ShortCut2 = 0
        end
        item
          Command = ecDeleteEOL
          ShortCut = 24665
          ShortCut2 = 0
        end
        item
          Command = ecUndo
          ShortCut = 16474
          ShortCut2 = 0
        end
        item
          Command = ecRedo
          ShortCut = 24666
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker0
          ShortCut = 16432
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker1
          ShortCut = 16433
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker2
          ShortCut = 16434
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker3
          ShortCut = 16435
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker4
          ShortCut = 16436
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker5
          ShortCut = 16437
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker6
          ShortCut = 16438
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker7
          ShortCut = 16439
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker8
          ShortCut = 16440
          ShortCut2 = 0
        end
        item
          Command = ecGotoMarker9
          ShortCut = 16441
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker0
          ShortCut = 24624
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker1
          ShortCut = 24625
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker2
          ShortCut = 24626
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker3
          ShortCut = 24627
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker4
          ShortCut = 24628
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker5
          ShortCut = 24629
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker6
          ShortCut = 24630
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker7
          ShortCut = 24631
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker8
          ShortCut = 24632
          ShortCut2 = 0
        end
        item
          Command = ecSetMarker9
          ShortCut = 24633
          ShortCut2 = 0
        end
        item
          Command = ecNormalSelect
          ShortCut = 24654
          ShortCut2 = 0
        end
        item
          Command = ecColumnSelect
          ShortCut = 24643
          ShortCut2 = 0
        end
        item
          Command = ecLineSelect
          ShortCut = 24652
          ShortCut2 = 0
        end
        item
          Command = ecTab
          ShortCut = 9
          ShortCut2 = 0
        end
        item
          Command = ecShiftTab
          ShortCut = 8201
          ShortCut2 = 0
        end
        item
          Command = ecMatchBracket
          ShortCut = 24642
          ShortCut2 = 0
        end>
      MaxLeftChar = 1024
      MaxUndo = 1024
      ReadOnly = False
      WantTabs = True
      SelEnd = 1
      SelStart = 1
      OnChange = MemoChange
      OnPaint = MemoPaint
      OnSpecialLineColors = MemoSpecialLineColors
      OnStatusChange = MemoStatusChange
    end
    object MessageList: TListBox
      Left = 0
      Top = 173
      Width = 379
      Height = 48
      Align = alBottom
      ItemHeight = 13
      TabOrder = 1
      OnDblClick = MessageListDblClick
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'pas'
    Filter = 'Inno Pascal unit (*.pas)|*.pas'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Left = 40
    Top = 48
  end
  object MainMenu1: TMainMenu
    Images = ImageList1
    OwnerDraw = True
    Left = 8
    Top = 48
    object FMenu: TMenuItem
      Caption = #25991#20214'(&F)'
      OnClick = FMenuClick
      object FNew: TMenuItem
        Caption = #26032#24314'(&N)'
        ImageIndex = 0
        ShortCut = 16462
        OnClick = FNewClick
      end
      object FOpen: TMenuItem
        Caption = #25171#24320'(&O)...'
        ImageIndex = 6
        ShortCut = 16463
        OnClick = FOpenClick
      end
      object FSave: TMenuItem
        Caption = #20445#23384'(&S)'
        ImageIndex = 7
        ShortCut = 16467
        OnClick = FSaveClick
      end
      object FSaveAs: TMenuItem
        Caption = #21478#23384'(&A)...'
        ImageIndex = 8
        OnClick = FSaveAsClick
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object FMRUSep: TMenuItem
        Caption = '-'
        Visible = False
      end
      object FExit: TMenuItem
        Caption = #36864#20986'(&X)'
        OnClick = FExitClick
      end
    end
    object EMenu: TMenuItem
      Caption = #32534#36753'(&E)'
      OnClick = EMenuClick
      object EUndo: TMenuItem
        Action = actUndo
        Caption = #25764#38144'(&U)'
        ImageIndex = 9
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object ECut: TMenuItem
        Action = actCut
        Caption = #21098#20999'(&T)'
        ImageIndex = 3
      end
      object ECopy: TMenuItem
        Action = actCopy
        Caption = #22797#21046'(&C)'
        ImageIndex = 2
      end
      object EPaste: TMenuItem
        Action = actPaste
        Caption = #31896#36148'(&P)'
        ImageIndex = 13
      end
      object EDelete: TMenuItem
        Caption = #21024#38500'(&L)'
        ImageIndex = 11
        OnClick = actDeleteClick
      end
      object ESelectAll: TMenuItem
        Caption = #20840#36873'(&A)'
        ShortCut = 16449
        OnClick = ESelectAllClick
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object EFind: TMenuItem
        Caption = #26597#25214'(&F)...'
        ImageIndex = 5
        ShortCut = 16454
        OnClick = EFindClick
      end
      object EFindNext: TMenuItem
        Caption = #32487#32493#26597#25214'(&N)'
        ShortCut = 114
        OnClick = EFindNextClick
      end
      object EReplace: TMenuItem
        Caption = #26367#25442'(&R)...'
        ShortCut = 16456
        OnClick = EReplaceClick
      end
    end
    object VMenu: TMenuItem
      Caption = #35270#22270'(&V)'
      OnClick = VMenuClick
      object VToolbar: TMenuItem
        Caption = #24037#20855#26639'(&T)'
        OnClick = VToolbarClick
      end
      object VCompilerMessages: TMenuItem
        Caption = #32534#35793#28040#24687'(&M)'
        OnClick = VCompilerMessagesClick
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object VD: TMenuItem
        Caption = #35843#35797#31383#21475'(&D)'
        object VDEventLog: TMenuItem
          Caption = #20107#20214#26085#24535'(&E)'
          OnClick = VDEventLogClick
        end
        object VDRegisters: TMenuItem
          Caption = #23492#23384#22120'(&R)'
          OnClick = VDRegistersClick
        end
      end
      object VEditorOptions: TMenuItem
        Caption = #32534#36753#22120#36873#39033'(&E)'
        Visible = False
        OnClick = VEditorOptionsClick
        object VEHorizCaret: TMenuItem
          Caption = 'Horizontal Caret Shape'
          OnClick = VEHorizCaretClick
        end
      end
    end
    object Project1: TMenuItem
      Caption = #39033#30446'(&P)'
      object PCompile: TMenuItem
        Action = actCompile
        Caption = #32534#35793'(&C)'
        ImageIndex = 12
      end
      object PBuild: TMenuItem
        Action = actBuild
        Caption = #26500#24314'(&B)'
        ImageIndex = 15
      end
    end
    object Run1: TMenuItem
      Caption = #36816#34892'(&R)'
      object RRun: TMenuItem
        Action = actRun
        Caption = #36816#34892'(&R)'
      end
      object RStop: TMenuItem
        Action = actStop
        Caption = #20572#27490'(&S)'
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object RStepOver: TMenuItem
        Action = actStepOver
      end
      object RRunToCursor: TMenuItem
        Action = actRunToCursor
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object RParameters: TMenuItem
        Caption = #21442#25968'(&P)...'
        OnClick = RParametersClick
      end
    end
    object mnTool: TMenuItem
      Caption = #24037#20855'(&T)'
      object mnEditOption: TMenuItem
        Caption = #32534#36753#22120#36873#39033
        OnClick = mnEditOptionClick
      end
    end
    object Help1: TMenuItem
      Caption = #24110#21161'(&H)'
      object HReadme: TMenuItem
        Caption = '&Readme.txt'
        OnClick = HReadmeClick
      end
      object HLicense: TMenuItem
        Caption = '&License.txt'
        OnClick = HLicenseClick
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object HAbout: TMenuItem
        Caption = #20851#20110'(&A)...'
        ImageIndex = 14
        OnClick = HAboutClick
      end
    end
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'pas'
    Filter = 'Inno Pascal unit (*.pas)|*.pas'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Left = 72
    Top = 48
  end
  object FindDialog: TFindDialog
    OnFind = FindDialogFind
    Left = 104
    Top = 48
  end
  object ReplaceDialog: TReplaceDialog
    OnFind = FindDialogFind
    OnReplace = ReplaceDialogReplace
    Left = 136
    Top = 48
  end
  object Highlighter: TSynPasSyn
    DefaultFilter = 'Pascal files (*.pas,*.dpr,*.dpk,*.inc)|*.pas;*.dpr;*.dpk;*.inc'
    CommentAttri.Foreground = clNavy
    CommentAttri.Style = [fsItalic]
    KeyAttri.Style = [fsBold]
    Left = 264
    Top = 32
  end
  object ActionList: TActionList
    Left = 136
    Top = 80
    object actRun: TAction
      Category = 'Run'
      Caption = '&Run'
      Hint = 'Run'
      ShortCut = 120
      OnExecute = actRunClick
    end
    object actStop: TAction
      Category = 'Run'
      Caption = '&Stop'
      Enabled = False
      Hint = 'Stop'
      ShortCut = 16497
      OnExecute = actStopClick
    end
    object actStepOver: TAction
      Category = 'Run'
      Caption = 'Step &Over'
      Hint = 'Step Over'
      ShortCut = 119
      OnExecute = actStepOverClick
    end
    object actCompile: TAction
      Category = 'Project'
      Caption = '&Compile'
      Hint = 'Compile'
      ShortCut = 16504
      OnExecute = actCompileClick
    end
    object actBuild: TAction
      Category = 'Project'
      Caption = '&Build'
      Hint = 'Build'
      OnExecute = actBuildClick
    end
    object actRunToCursor: TAction
      Category = 'Run'
      Caption = 'Run to &Cursor'
      Hint = 'Run to Cursor'
      ShortCut = 115
      OnExecute = actRunToCursorExecute
    end
    object actUndo: TAction
      Category = 'Edit'
      Caption = '&Undo'
      Hint = 'Undo'
      ShortCut = 16474
      OnExecute = actUndoClick
    end
    object actCut: TAction
      Category = 'Edit'
      Caption = 'Cu&t'
      Hint = 'Cut'
      ShortCut = 16472
      OnExecute = actCutClick
    end
    object actCopy: TAction
      Category = 'Edit'
      Caption = '&Copy'
      Hint = 'Copy'
      ShortCut = 16451
      OnExecute = actCopyClick
    end
    object actPaste: TAction
      Category = 'Edit'
      Caption = '&Paste'
      Hint = 'Paste'
      ShortCut = 16470
      OnExecute = actPasteClick
    end
    object actDelete: TAction
      Category = 'Edit'
      Caption = 'De&lete'
      Hint = 'Delete'
      OnExecute = actDeleteClick
    end
  end
  object MemoPopup: TPopupMenu
    OwnerDraw = True
    Left = 8
    Top = 80
    object MPUndo: TMenuItem
      Action = actUndo
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object MPCut: TMenuItem
      Action = actCut
    end
    object MPCopy: TMenuItem
      Action = actCopy
    end
    object MPPaste: TMenuItem
      Action = actPaste
    end
    object MPDelete: TMenuItem
      Action = actDelete
    end
  end
  object ImageList1: TImageList
    Left = 104
    Top = 80
    Bitmap = {
      494C010110001300040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001001000000000000028
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      3967000000040004000000008310396700000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF7FDE7B
      BD779C739C73BD77DE7BDE7BFF7F000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D65A
      210808256B316A318B35292584100000DE7B00000C46A839873566310000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000DE7B1863C618
      630C4208630C4A299C737B6F630CA514F75E0000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000F75E6310
      6A2DCD3D304A0F462F4A304E6A310000524A00009977D062D062D0620000FF7F
      5A6B94529452FF7F00009452B556FF7F000000000000524ACD39000049290F42
      EE3DEF3DCE394A2D2104C618AD359C7321040000000000000000000000000000
      000000007B6F000000000000BD7700000000000000000000000000000000D65E
      1046D462D45EB35EB35ED45E2F4A492908210000BA77D0626E52D0620000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000D65A0000C4308959176B
      7B6F5A6B1867EE51E53CE53CE71C4A2921040000000000000000000000000000
      000000000000036D036DC2540000000000000000000000000000000000005A6F
      376F376B376F376F5873F566386FCD3D000000009A77D062D062566F0000FF7F
      F75E94529452F75EFF7F00005A6BFF7F000000003967000006498961EC65D472
      BD7BDE7B9C7BED5D685DE53CE53C0000EF3D0000000000000000000000000000
      000000000000EC71EC71036D0000000000000000000000000000000000007B73
      9356997BF666376B596F596F386F304A000000009A77D0626E52D0620000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000104249292F660D6A926E0E5E
      0000000000007266ED5D6859AB4508216B2D0000000000000000000000000000
      000000000000D476EC71036D0000000000000000000000000000000000009C77
      514ABB7B0F42DD7F304ABC7BEE3D1042000000009A77D062D062566F0000FF7F
      F75E9452F75E5A6BF75E9452FF7FFF7F000000006B2DF65E9C77166F8B450000
      9C73FF7F186300007262F66AD65A8C310000000000005A6B0000000039670000
      000000007B6F0000000000009C7300000000000000000000000000000000DE7B
      514EBC7F51520000AD390000AC399452000000009A77D0622D4ED0620000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000000000009C737B6F3867E71CF75E
      FF7F000000001863E71CBD7B3867CE39000000005A6B0000FA4EB02500003967
      00000000000000000000000000000000000000000000000000000000D65A0000
      2925DC7FAC3900000000BD7721040000B55600009A77D062D062566F0000FF7F
      F75E9452F75EFF7F000094525A6BFF7F000000000000DD7B5A6BB55A00007B6F
      FF7F00000000FF7F0000DD7B5A6FCE3900005A6B0000553AFA4EB025B0250000
      39670000000000000000D65A0000000000000000000000000000000000000000
      C618BC7F29250000CE39000018630000000000009A77D0622D4ED0620000FF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F000000000000DD7B396B5152E71C3967
      DE7BFF7FFF7F9C73E71CBC7B9B730F420000A610553A553AFA4EB025B025B025
      00005A6B0000000094520000B556000000000000000000000000000000000000
      C61CDD7F00000000000000000000000000000000BA77D062D062566F472D0000
      00000000000000000000000000000000000000006B2D9C73B36A4851E71C4A29
      39677B6FF75E0000CC59726EF66E8B3100000000763E553AFA4EB025B025B025
      B0250000BD77B556000000000000F75E00000000536F0E6F0E6F0E6F0000333E
      A514FF7F00000000CB69E364E364E36400000000BA77D0622D4EC4188310A418
      8310C41889356E52CA3D0000FF7FFF7FFF7F0000CE394929D572CC61E5406A31
      63100000492927412F6ACB6569594931CE393967E918553AFA4EB025B025B025
      0000BD7700000000000000000000000000000000957B507750770E6F00007542
      C61C000000000000EC75036D036DE364000000009A77D062C418B356BC77BC77
      B456AC3583100C46CA3D0000FF7FFF7FFF7F000039670000F66ED4722F62F762
      D55AF7629B73387389616861C4308410F75E00000000763EFA4EB025B0250000
      BD77000000000000000000000000000000000000957B507750770E6F00007542
      0A1500000B190000EC75036D036DE36400000000DC7BBA77BB77A514EE3D6B2D
      49298310CD3D504A0D460000FF7FFF7FFF7F00000000734E0000B46E59779C77
      9C739C77BD77BD77506A064900003146000000001863E814FA4EB0250000BD77
      000000000000000000007B6F0000000000000000957B957B957B536F00007542
      75427542333E0000EC75EC75EC75CB6900000000000000000000000031469C73
      AD3500000000000000000000FF7FFF7FFF7F000000009452524A0000CE395A6F
      DD7BDD7B9C73F65E8C318414734E524A0000000000000000FA4E0000BD770000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF7FFF7FFF7FFF7FFF7F84108410
      4208FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000F75E00000000F75E6B2D0000
      00000000000000001042F75E00000000FF7F000000009C730000BD7700000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF7FFF7FFF7FFF7F000000000000000000000000000000000000EF3D0000
      0000000000000000000000000000314600000000000000000000B55600000000
      2104000000000000D65A000000000000000000000000B5560000000000000000
      000000000000000000000000000000004A290000000000000000000000000000
      00001863F75E1863FF7F00000000000000000000000000000000000000005A6B
      0000000000000000000000005A6B0000000000000000D65A0000000007212F46
      ED3D0721AB35C518A51400001863000000000000B55600004A2908215A6B3967
      39671863F75E1863734E0821CE3984100000000000000000000000000000DE7B
      186342084208F75E000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008410072130466A2D7A6F
      B3566A2D504A28256A2DA414831000000000000000008C3110426B2D3967A514
      8C3118631863D65A524A6B2D31462A25000000000000000000000000DE7B1863
      4208D13942081863186318637B6F7B6F00000000000000000000000000009C73
      6B2D4A29524A00006B2D8C3100000000000000000000A414EE3D7A6F30467A6F
      B3566A2D92522825ED3D0721A4140000000000000000F75E31426B2D1863C718
      6B2D18631863F75E524A6B2D31462A2500000000000000000000DE7B18634208
      D1399652D139420842084208630C7B6F00000000000000000000000000000000
      00000000000000000000000000000000000000000000A414B4567A6F30467A6F
      B3566A2D92522825ED3D4829A4140000000000000000F75E31466B2D18638410
      E71C5A6B1863F75E524A6B2D31462A250000000000000000DE7B18634208D139
      544AD139D139D139D13942087B6F00000000EF3D000000000000000000000000
      4A29292500009452AD35D65A00000000000000000000A414B4567A6F2F46596B
      2F46492D504A2725ED3D4829A4140000000000000000F75E3146CE3939671863
      3967BD775A6B5A6B9452CE3910422A25000000000000000018634208D1393346
      D139D139D139D13900007B6F00000000000000005A6B00000000000000000000
      00000000000000000000000000000000000000000000A4149352D55ACC399252
      CC390621AB35E51CAB35E61C84100000000000000000F75E3146EF39CE39AD35
      AD35AD35AD35AD35AD35AD3510422A2500000000000018634208D1391342D139
      D139D139D13900007B6F00000000000000000000000000000000000000000000
      8C3129254A290000AD35EF3D00000000000000000000A41492522825C618A414
      A514210421044108E71CC51883100000000000000000F75E31466C298D318C2D
      8C2D8C2D8C2D8C2D8C2D4B2910422A250000000000004208D139F241D139D139
      D139D13900007B6F0000000000000000000000009C736B2D4A29524A00000000
      00000000000000000000000000000000000000000000630CC61828258C35AC39
      AC35AB35492DE61C4208620C630C0000000000000000F75E3146AE3518631863
      18631863186318631863AE3510422A2500000000000000004208D1391342D139
      D139D139D13900007B6F0000000000000000000000000000000000000000D65A
      0000000000000000000000005A6B000000000000EF3D4829CC39724E99739977
      9973596B2F46CC398B350721A414EF3D000000000000F75E3146AE3518631863
      18631863186318631863AE3510422A25000000000000000000004208D1393346
      D139D139D139D13900007B6F000000000000000000004A29292500004A290000
      0000000000000000000000000000EF3D0000000000002E46BA7BFD7FDD7FDD7F
      DD7FDC7BBB77796F714E8A31492D0000000000000000F75E3146AE3518631863
      18631863186318631863AE3510422A250000000000000000000000004208D139
      544AD139D139D139D13942087B6F000000000000000000000000000000000000
      000000000000000000000000000000000000000000009A77DD7BDC7BBB779A73
      576B1463B256F4621563D35ECC3D0000000000000000F75E3146AE3518631863
      18631863186318631863AE3510424A2900000000000000000000000000004208
      D1399652D139420842084208630C00000000000000008C3129254A290000AD35
      EF3D000000000000000000000000000000000000A514576B786F9973D45E8B31
      E61CE61C8A31704ED35AD35A91520000000000000000186331462931CD61CC61
      CC61CC61CC61CC61CD65293931466B2900000000000000000000000000000000
      4208D13942081863000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000186342081463F462EC3D0825
      8410C5180E4271529152915200007B6F0000000000005A6BF75E2935306A306A
      306A306A306A506A306A4A39D65AD65A00000000000000000000000000000000
      0000420842080000000000000000000000000000D65A00000000000000000000
      00005A6B0000000000000000000000000000000000001863000000000E42BB7B
      DD7BBC771763482900000000D65A0000000000004A2900000000000000000000
      000000000000000000000000000000004A290000000000000000000000000000
      000000000000000000000000000000000000EF3D000000000000000000000000
      00000000EF3D000000000000000000000000000000000000BD77D65A21040821
      2925E71CA514000018637B6F0000000000000000F75E00000000000000000000
      0000000000000000000000001863000000000000524A00000000000000000000
      000000000000000000000000734E000000000000000000000000000000000000
      0000000000000000000000000000000000000000B55600000000000000000000
      00000000000000000000000000004A290000000000001863F75EF75EF75EF75E
      F75EF75EF75EF75EF75E9452A51400000000000000001863F75EF75EF75EF75E
      F75EF75EF75EF75EF75E00000000000000009452000000000000000000000000
      000000000000000000000000000094520000B55600004A2908215A6B39673967
      1863F75E1863734E0821CE398410000000000000000000009C739C739C739C73
      9C739C739C739C739C73F75E0000000000000000000000009C739C739C739C73
      9C739C739C739C7300000000000000000000000017639152914E714E714E714E
      704A504A504A504A4F462F462F460000000000008C3110426B2D3967A5148C31
      18631863D65A524A6B2D31462A25000000000000000000009C739C739C739C73
      9C739C739C739C739C73F75E0000000000000000000000009C739C739C739C73
      9C739C739C730000000000000000000000000000796BF45AD45AD35AD35AB356
      B356B256B25292529252914E4F46000000000000F75E31426B2D1863C7186B2D
      18631863F75E524A6B2D31462A2500000000000000000000BD7718639C731863
      000000009C739C739C73F75E000000000000000000000000BD77524A28252104
      21040821D65A00000000F75E0000000000000000796BF55EF45EF45AF45AD45A
      D45AD356B356B356B2569252504A000000000000F75E31466B2D18638410E71C
      5A6B1863F75E524A6B2D31462A2500000000000000000000BD77BD77BD77BD77
      00000225000000009C73F75E000000000000000000000000BD77C5180F429252
      504ACC39000000009C73F75E0000000000000000796B1563155FF55EF55EF45E
      F45ED45AD45AD35AD356B356714E000000000000F75E3146CE39396718633967
      BD775A6B5A6B9452CE3910422A2500000000000000000000BD77186318631863
      BD770000AE6A6631000018630000000000000000000000000821504AF45EB356
      714E504ACC392825BD7718630000000000000000796B3663166316631563155F
      F55EF55EF45EF45AD45AD45A914E000000000000F75E3146EF39CE39AD35AD35
      AD35AD35AD35AD35AD3510422A2500000000000000000000BD77BD77BD77BD77
      BD770000467BC976252D0000000000000000000000000000210436677A6FD45A
      B356714E2F462104BD7718630000000000000000796B36673667166316631563
      1563F55EF55EF55EF45ED45A9252000000000000F75E31466C298D318C2D8C2D
      000000008C2D8C2D4B2910422A2500000000000000000000BD7739673967BD77
      396739670000677FC87A673100007B6F00000000000000002104576BFF7F1563
      D45A9252504A2004BD7718630000000000000000586B3667366736673667F55E
      F55EF55E155FF55EF55EF45E9252000000000000F75E3146AE35186318631863
      0000022500000000AE3510422A2500000000000000000000BD77BD77BD77BD77
      BD77BD77BD770000A77FC87A6635000039670000000000000721714E9A73BC77
      1563D35A0E422825BD7718630000000000000000596B596B596B596B596B596B
      796F16631663155FF55EF55E9252000000000000F75E3146AE35186318631863
      18630000AE6A6631000010422A2500000000000000000000DE7B396739673967
      3967BD77396739670000A77FC87A66310000000000000000DE7BC518714E3767
      15632F46C618BD77BD7718630000000000000000000000000000000000000000
      9352796F596B586B586B586BF55E000000000000F75E3146AE35186318631863
      18630000467BC976252D00002A2500000000000000000000DE7BDE7BDE7BDE7B
      DE7BDE7B9C73104229250000877FC97A0000000000000000DE7BDE7B07212104
      210428259C7310422925420800000000000000004F46AA35AA35AA31AA316929
      0000000000000000000000000000000000000000F75E3146AE35186318631863
      186318630000677FC87A6731000000000000000000000000DE7B396739673967
      DE7BDE7BB556CE396B2D082100000000D65A000000000000DE7BD65AB556CE39
      CE39F75EB556CE396B2D082100000000000000004F46CC39CC39CC39CC39CB39
      CB35CC39CC39CC39CC39CC39AB31000000000000186331462931CD61CC61CC61
      CC61CC61CC610000A77FC87A663500000000000000000000DE7BDE7BDE7BDE7B
      DE7BDE7B1863FF7FB55600009C7300000000000000000000DE7BDE7BDE7BDE7B
      DE7BDE7B1863FF7FB5560000D65A00000000000091520D420D420D420D420000
      00000000000000000000000000009452000000005A6BF75E2935306A306A306A
      306A306A506A306A0000A77FC87A463100000000000000000000000000000000
      000000009C73186300007B6F0000000000000000000000000000000000000000
      000000009C7318630000B55600000000000000004F464F464F464F46AB310000
      0000000000000000000000000000000000004A29000000000000000000000000
      000000000000000000000000877FC97A000000007B6F00000000000000000000
      00000000000000009C7300000000000000000000B55600000000000000000000
      0000000000000000D65A0000000000000000B55600000000000000000000D65A
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000524A00000000000000000000
      000000000000000000000000734E00000000000000000000000000000000BD77
      BD770000000000000000000000000000000000000000000000000000EF3D0000
      0000000000000000000000000000314600000000000000000000000000000000
      000000000000000000000000000000000000000000001863F75EF75EF75EF75E
      F75EF75EF75EF75EF75E9452A51400000000000000000000945242084A29F75E
      5A6B7B6F00000000000000000000000000000000000000000000000000005A6B
      0000000000000000000000005A6B0000000000000000945200000000D65A0000
      00000000945200000000D65A0000000000000000000000009C739C739C739C73
      9C739C739C739C739C73F75E00000000000000000000DE7BC61CC25901310000
      10429C739C73BD77000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000009C739C7300000000
      0000000000009C739C7300000000000000000000000000009C739C739C739C73
      9C739C739C739C739C73F75E000000000000000000000000013102620262225E
      200408214A29D65A9C7300000000000000000000000000000000000000009C73
      6B2D4A29524A00006B2D8C310000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000BD779C739C739C73
      9C739C739C739C739C73F75E0000000000000000DE7B2004C25102626266826A
      0252225A025200001863BD770000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000018639C730000D65A
      0000D65A000018639C730000000000000000000000000000BD77BD77BD77BD77
      BD77BD77BD779C739C73F75E00000000000000001863A01862664266A26AA26A
      826A6266C26E2139082194527B6FBD770000EF3D000000000000000000000000
      4A29292500009452AD35D65A00000000000000000000D65A00000000C6186B2D
      0000CE392925000000009452000000000000000000000000BD77BD77BD77BD77
      BD77BD77BD77BD77BD7718630000000000000000D65AA018E26E22732273A26A
      826AA26AA26A82660252E128C618F75EBD7700005A6B00000000000000000000
      000000000000000000000000000000000000000000000000000000009452C618
      42088410524A000000000000000000000000000000000000BD77BD77BD77BD77
      BD77BD77BD77BD77BD7718630000000000000000C514724AD452734EC862A26A
      826AC26E42666266E26EC26E413D0000EF3D0000000000000000000000000000
      8C3129254A290000AD35EF3D0000000000000000000000000000000000001763
      00001767A514000000000000000000000000000000000000BD77BD77BD77BD77
      BD77BD77BD77BD77BD771863000000000000BD778410D556D452B24A2E3A0D36
      0262826A026FC26EE259E1248410524ADE7B00009C736B2D4A29524A00000000
      000000000000000000000000000000000000000000000000000000009B73724E
      0000386BF65E000000000000000000000000000000000000BD77BD77BD77BD77
      BD77BD77BD77BD77BD7718630000000000005A6BE71CE71C8B2D924A0F3E0D36
      AB2D883DC355413D84109452000000000000000000000000000000000000D65A
      0000000000000000000000005A6B00000000000000000000D65A29255A6F0000
      0000000017674929D65A0000000000000000000000000000DE7BBD77BD77BD77
      BD77BD77BD77BD77BD7718630000000000000000DE7B42082E3A0F3E0F3E0F3E
      AA298825671D000000000000000000000000000000004A29292500004A290000
      0000000000000000000000000000EF3D00000000000000002925386717630000
      000000009B73725229250000000000000000000000000000DE7BDE7BDE7BDE7B
      DE7BDE7B9C731042292542080000000000000000104228212E3A872142080000
      00000D36E4148C31DE7B00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7300000000
      000000000000176700000000000000000000000000000000DE7BDE7BDE7BDE7B
      DE7BDE7BB556CE396B2D08210000000000000000A51450422E3AE5148C310000
      524A00000000524A00000000000000000000000000008C3129254A290000AD35
      EF3D0000000000000000000000000000000000000000000000007A6F00000000
      000000000000386B00000000000000000000000000000000DE7BDE7BDE7BDE7B
      DE7BDE7B1863FF7FB5560000D65A0000000000004208165BD45262085A6B0000
      0000DE7BBD77DE7B000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000D65A000000000000
      0000000000000000D65A00000000000000000000000000000000000000000000
      000000009C7318630000B5560000000000000000C514165BE618104200000000
      0000000000000000000000000000000000000000D65A00000000000000000000
      00005A6B00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000B55600000000000000000000
      0000000000000000D65A00000000000000000000CE396208CE39000000000000
      000000000000000000000000000000000000EF3D000000000000000000000000
      00000000EF3D0000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FE018001F803FFFFFC000000C000FFFF
      F80000108000FF83F80000008000FF83F80000088000FF83F80000008000FF83
      F80000008000C383F828000080C081FFF820001080C000F7FE03000080000063
      0000000180000001000000008000007700800000800080F70000000080008187
      000080088000C3FF000000008000C7FFFFFFFF0FF801F00FC000FF0FF9F9C003
      8000FC1FFBFDC0038000F801F825C0038000F001FBFDC0038000E0030245C003
      8000E0073BFDC0038000C00F7A25C0038000C01F03FDC0038000E00F79F98001
      8000F007480180018000F8037FBF80018000FC0344BF80018000FE1F7FBF8001
      8000FF3F3F3FC0038000FFFF003FE00780038003FFFF80018003800300010001
      A003A00300010001A003A00300010001A003A00300010001A003A00300010001
      A003A00300010001A003A00300010001A001A00300010001A000A00300010001
      A000A00300010001A000A00300010001A000A00300010001A003A00300010000
      BF87BF8701FF0000800F800F01FFFFF98003FCFFF801FFFF8003E07FF9F9C387
      A003C01FFBFDC387A003C00FF825DBB7A0038007FBFDC107A00380010245C107
      A00380003BFDF83FA00380007A25F83FA003000003FDF01FA003000779F9E10F
      A003801F4801E10FA003800F7FBFE38FA003821F44BFE38FA003831F7FBFE7CF
      BF8787FF3F3FF7DF800F8FFF003FFFFF}
  end
  object XPMenu1: TXPMenu
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenuText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = []
    Color = clBtnFace
    IconBackColor = clBtnFace
    MenuBarColor = clBtnFace
    SelectColor = clHighlight
    SelectBorderColor = clHighlight
    SelectFontColor = clMenuText
    DisabledColor = clInactiveCaption
    SeparatorColor = clBtnFace
    CheckedColor = clHighlight
    IconWidth = 24
    DrawSelect = True
    UseSystemColors = True
    OverrideOwnerDraw = False
    Gradient = False
    FlatMenu = False
    AutoDetect = False
    Active = True
    Left = 73
    Top = 83
  end
end
